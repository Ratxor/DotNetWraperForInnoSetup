.Net Wraper For Inno Setup
========================

.NET Wrapper for Inno Setup


Welcome to the .NET Wraper For InnoSetup (http://www.jrsoftware.org/isinfo.php)

It is a .NET Class library project that gives .NET Developers the pleasure of using Inno Setup without learning any Pascal scripting language. Not only that, this wrapper .NET Interface will allow the .NET Programmers to create installer file automatically as part of Continuous Integration directly using .NET Language. The .NET Developers will be able to call a method named BuildSetupFile() in a Wrapper class named InnoSetupService. Yes, that will build the Installer exe file.  Please check the Journal http://www.spicelogic.com/Journal/DotNet-Wrapper-for-Inno-Setup-8 for details about the features and how to use with code examples.
